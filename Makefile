DOCKER=nvidia-docker
VOLUME_PATH=wunder_volume_nlp

IMAGE=nlp_translator_dev
CONTAINER_NAME=wunderbar

EPOCHS?=10

build:
	$(DOCKER) build -t $(IMAGE) .

train:
	$(DOCKER) run \
	--name=$(CONTAINER_NAME) \
	--rm \
	-it \
	-v $(VOLUME_PATH):/data \
	$(IMAGE) \
		python nlp-scripts/train.py \

preprocess-data:
	$(DOCKER) run \
	--name=$(CONTAINER_NAME) \
	--rm \
	-it \
	-v $(VOLUME_PATH):/data \
	$(IMAGE) \
	python nlp-scripts/preprocess.py \


translate:
	$(DOCKER) run \
	--name=$(CONTAINER_NAME) \
	--rm \
	-it \
	-v $(VOLUME_PATH):/data \
	$(IMAGE) \
	python nlp-scripts/translate.py \

evaluate-model:
	$(DOCKER) run \
	--name=$(CONTAINER_NAME) \
	--rm \
	-it \
	-v $(VOLUME_PATH):/data \
	$(IMAGE) \
	python nlp-scripts/evaluate-model.py \

clean:
	docker volume rm $(VOLUME_PATH)
	rm -rf volume/

clear: clean

all: build run

#train:
#	echo $(EPOCHS)
	# TODO invoke training and pass param

.PHONY: build run clean clear train preprocess-data
