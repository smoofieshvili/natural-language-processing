FROM tensorflow/tensorflow:latest-gpu-py3

RUN mkdir /data

WORKDIR /

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

COPY run.sh .
COPY ./scripts/ ./nlp-scripts/
COPY ./dataset/ ./data/nlp-dataset/
