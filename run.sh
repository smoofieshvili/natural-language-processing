#!/usr/bin/env bash

if [! -e data/english-german.pkl]; then
	python nlp-scripts/preprocess-data.py
fi

if [! -e data/model.h5]; then
	python nlp-scripts/main.py
fi

python nlp-scripts/translate.py
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to run main.py: $status"
  exit $status
fi

