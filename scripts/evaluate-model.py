import mein_utils
from preprocess import load_clean_sentences
from nltk.translate.bleu_score import corpus_bleu
import keras

# evaluate the skill of the model
def evaluate_model(model, tokenizer, sources, raw_dataset):
	actual, predicted = list(), list()
	print('Example results:')
	for i, source in enumerate(sources):
		# translate encoded source text
		source = source.reshape((1, source.shape[0]))
		translation = mein_utils.predict_sequence(model, tokenizer, source)
		raw_target, raw_src = raw_dataset[i]
		if i < 10:
			print('source=[%s], expected=[%s], predicted=[%s]' % (raw_src, raw_target, translation))
		actual.append(raw_target.split())
		predicted.append(translation.split())
	# calculate BLEU score
	print('\nBLEU score:')
	print('BLEU-1: %f' % corpus_bleu(actual, predicted, weights=(1.0, 0, 0, 0)))
	print('BLEU-2: %f' % corpus_bleu(actual, predicted, weights=(0.5, 0.5, 0, 0)))
	print('BLEU-3: %f' % corpus_bleu(actual, predicted, weights=(0.3, 0.3, 0.3, 0)))
	print('BLEU-4: %f' % corpus_bleu(actual, predicted, weights=(0.25, 0.25, 0.25, 0.25)))

def main():
	# load datasets
	dataset = load_clean_sentences('data/english-german-both.pkl')
	train = load_clean_sentences('data/english-german-train.pkl')
	test = load_clean_sentences('data/english-german-test.pkl')

	# prepare source tokenizer
	src_tokenizer = mein_utils.create_tokenizer(dataset[:, 1])
	src_length = mein_utils.max_length(dataset[:, 1])

	# prepare destination tokenizer
	dst_tokenizer = mein_utils.create_tokenizer(dataset[:, 0])

	# prepare data
	train_src = mein_utils.encode_sequences(src_tokenizer, src_length, train[:, 1])
	test_src = mein_utils.encode_sequences(src_tokenizer, src_length, test[:, 1])

	# load model
	model = keras.models.load_model('data/model.h5')

	# test on some training sequences
	print('\n\nTRAINING DATA')
	evaluate_model(model, dst_tokenizer, train_src, train)

	# test on some test sequences
	print('\n\nTEST DATA')
	evaluate_model(model, dst_tokenizer, test_src, test)

if __name__ == '__main__':
    main()
