import string
import re
from pickle import dump, load
from unicodedata import normalize
import numpy as np


def file_to_string(filename):
    file = open(filename, mode='rt', encoding='utf-8')
    text = file.read()
    file.close()
    return text


def to_pairs(doc):
    lines = doc.strip().split('\n')
    pairs = [line.split('\t') for line in lines]
    return pairs


def clean_pairs(lines):
    cleaned = []
    re_print = re.compile('[^%s]' % re.escape(string.printable))
    table = str.maketrans('', '', string.punctuation)
    umlauts_table = str.maketrans({u"\u00c4": u"Ae", u"\u00e4": u"ae", u"\u00d6": u"Oe", u"\u00f6": u"oe", u"\u00dc": u"Ue", u"\u00fc": u"ue", u"\u00df": u"ss"})
    for pair in lines:
        clean_pair = []
        for line in pair:
            #line = [word.translate(umlauts_table) for word in line]
            line = line.translate(umlauts_table)
            line = normalize('NFD', line).encode('ascii', 'ignore')
            line = line.decode('UTF-8')
            # tokenize on white space
            line = line.split()
            # convert to lowercase
            #line = [word.lower() for word in line]
            # remove punctuation from each token
            line = [word.translate(table) for word in line]
            # remove non-printable chars form each token
            line = [re_print.sub('', w) for w in line]
            # remove tokens with numbers in them
            line = [word for word in line if word.isalpha()]
            # store as string
            clean_pair.append(' '.join(line))
        cleaned.append(clean_pair)
    return np.array(cleaned)


def save_clean_data(sentences, filename):
    dump(sentences, open(filename, 'wb'))
    print('Saved: %s' % filename)


def load_clean_sentences(filename):
	return load(open(filename, 'rb'))


def clean():
    print('CLEANING DATA...')
    filename = 'data/nlp-dataset/eng-ger.txt'
    doc = file_to_string(filename)
    # split into english-german pairs
    pairs = to_pairs(doc)
    # clean sentences
    clean_data = clean_pairs(pairs)
    # save clean pairs to file
    save_clean_data(clean_data, 'data/english-german.pkl')
    # spot check
    print('\nExample of cleaned data:')
    for i in range(10):
        print('[%s] => [%s]' % (clean_data[i,0], clean_data[i,1]))


def prepare_sets():
    print('\nSPLITTING DATA...')
    dataset = load_clean_sentences('data/english-german.pkl')

    dataset_size = 10000 #len(dataset)
    subset = dataset[:dataset_size]
    np.random.shuffle(subset)
    train, test = subset[:int(dataset_size * 0.9)], subset[int(dataset_size * 0.9):]
    save_clean_data(subset, 'data/english-german-both.pkl')
    save_clean_data(train, 'data/english-german-train.pkl')
    save_clean_data(test, 'data/english-german-test.pkl')


if __name__ == '__main__':
    clean()
    prepare_sets()
