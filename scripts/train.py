import model_builder
from preprocess import load_clean_sentences
import mein_utils
from keras.callbacks import ModelCheckpoint, Callback
import time

start = time.time()

class TimeHistory(Callback):
    def on_epoch_end(self, batch, logs={}):
        current = time.time()
        time_count = current - start
        print(str(round(time_count, 3)) + ' seconds\n\n')

def main():
    dataset = load_clean_sentences('data/english-german-both.pkl')
    train = load_clean_sentences('data/english-german-train.pkl')
    test = load_clean_sentences('data/english-german-test.pkl')

    print("dataset size: ", len(dataset), len(train), len(test))

	# prepare source tokenizer
    src_tokenizer = mein_utils.create_tokenizer(dataset[:, 1])
    src_vocab_size = len(src_tokenizer.word_index) + 1
    src_length = mein_utils.max_length(dataset[:, 1])
    print('Source vocabulary size: %d' % src_vocab_size)
    print('Source Max Length: %d' % (src_length))

	# prepare destination tokenizer
    dst_tokenizer = mein_utils.create_tokenizer(dataset[:, 0])
    dst_vocab_size = len(dst_tokenizer.word_index) + 1
    dst_length = mein_utils.max_length(dataset[:, 0])
    print('Destination vocabulary size: %d' % dst_vocab_size)
    print('Destination Max Length: %d' % (dst_length))

    del dataset

    # prepare training data
    train_src = mein_utils.encode_sequences(src_tokenizer, src_length, train[:, 1])
    train_dst = mein_utils.encode_sequences(dst_tokenizer, dst_length, train[:, 0])
    del train
    train_dst = mein_utils.encode_output(train_dst, dst_vocab_size)
    # prepare validation data
    test_src = mein_utils.encode_sequences(src_tokenizer, src_length, test[:, 1])
    test_dst = mein_utils.encode_sequences(dst_tokenizer, dst_length, test[:, 0])
    del test
    test_dst = mein_utils.encode_output(test_dst, dst_vocab_size)

    print("defining model")
    # define model
    model = model_builder.build_model(src_vocab_size, dst_vocab_size, src_length, dst_length, 256)
    model.compile(optimizer='adam', loss='categorical_crossentropy')
    # summarize defined model
    print(model.summary())
    filename = 'data/model.h5'
    checkpoint = ModelCheckpoint(filename, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
    model.fit(train_src, train_dst, epochs=100, batch_size=64, validation_data=(test_src, test_dst), callbacks=[checkpoint, TimeHistory()], verbose=2)

    model.save_weights('data/weights.h5')


if __name__ == '__main__':
    main()
