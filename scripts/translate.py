import model_builder
from preprocess import load_clean_sentences, clean_pairs
import mein_utils
import numpy as np
import keras

def main():
    # load datasets
    dataset = load_clean_sentences('data/english-german-both.pkl')
    
    # prepare source tokenizer
    src_tokenizer = mein_utils.create_tokenizer(dataset[:, 1])
    src_length = mein_utils.max_length(dataset[:, 1])

    # prepare destination tokenizer
    dst_tokenizer = mein_utils.create_tokenizer(dataset[:, 0])

    # load previously trained model
    model = keras.models.load_model('data/model.h5')

    to_translate = clean_pairs([["was ist meine Bestimmung?"], ["Feuer"],
                                ["Wasser"], ["wunderbar"], ["Guten Tag"]])[:, 0]

    to_translate_encoded = mein_utils.encode_sequences(src_tokenizer, src_length, to_translate)

    print("\nExample translations:")
    for index, source in enumerate(to_translate_encoded):
        source = source.reshape((1, source.shape[0]))
        translation = mein_utils.predict_sequence(model, dst_tokenizer, source)
        print("source=%s | translation=%s" % (to_translate[index], translation))

    print("\nTry other sentences (press Ctrl+C to exit):")
    while True:
        sentence = input("\nEnter sentence to tanslate: ")
        sentence = clean_pairs([[sentence]])[:, 0]
        sentence_encoded = mein_utils.encode_sequences(src_tokenizer, src_length, sentence)
        translation = mein_utils.predict_sequence(model, dst_tokenizer, sentence_encoded)
        print("source=%s | translation=%s" % (sentence[0], translation))


if __name__ == "__main__":
    main()
