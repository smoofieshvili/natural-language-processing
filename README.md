# Neural Machine Translation project based on Keras API


### Setup instructions
1. Install nvidia-docker2
2. Install nvidia drivers
3. Install cuda-9 according to [official documentation](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/) 
4. Install cudnn-7 according to [official documentation](https://docs.nvidia.com/deeplearning/sdk/cudnn-install/)



### Building image
```
make build
```

### Preprocessing data

```
make preprocess-data
```

### Training model

```
make run
```

### Running trained model

```
make translate
```

### Cleaning volume state

```
make clean
```
